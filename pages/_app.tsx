// import '../styles/globals.css'
import "../styles/assets/plugins/global/plugins.bundle.css";
import "../styles/assets/css/style.bundle.css";
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
